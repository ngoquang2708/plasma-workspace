msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-27 02:04+0000\n"
"PO-Revision-Date: 2023-05-22 13:57\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/plasma-workspace/klipper.pot\n"
"X-Crowdin-File-ID: 42995\n"

#: configdialog.cpp:80
#, kde-format
msgid "Selection and Clipboard:"
msgstr "选中内容和剪贴板："

#: configdialog.cpp:87
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"When text or an area of the screen is highlighted with the mouse or "
"keyboard, this is the <emphasis>selection</emphasis>. It can be pasted using "
"the middle mouse button.<nl/><nl/>If the selection is explicitly copied "
"using a <interface>Copy</interface> or <interface>Cut</interface> action, it "
"is saved to the <emphasis>clipboard</emphasis>. It can be pasted using a "
"<interface>Paste</interface> action. <nl/><nl/>When turned on this option "
"keeps the selection and the clipboard the same, so that any selection is "
"immediately available to paste by any means. If it is turned off, the "
"selection may still be saved in the clipboard history (subject to the "
"options below), but it can only be pasted using the middle mouse button."
msgstr ""
"当您使用鼠标或者键盘将屏幕的某个部分的内容高亮显示时，它们将成为<emphasis>选"
"中内容</emphasis>。您可以使用鼠标中间键粘贴选中的内容。<nl/><nl/>如果您通过软"
"件界面对选中内容进行了<interface>复制</interface>或者<interface>剪切</"
"interface>操作，它们将被保存到<emphasis>剪贴板</emphasis>中。剪贴板中的内容可"
"以通过<interface>粘贴</interface>操作进行粘贴。<nl/><nl/>启用此选项后，选中内"
"容和剪贴板内容将被一视同仁，任何选中的内容都可以立即通过任意方式进行粘贴。禁"
"用此选项后，选中内容可能还会被保存到剪贴板的历史记录中 (通过下面的选项控制)，"
"但只能通过鼠标中间键进行粘贴。"

#: configdialog.cpp:106
#, kde-format
msgid "Clipboard history:"
msgstr "剪贴板历史："

#: configdialog.cpp:112
#, kde-format
msgctxt "Number of entries"
msgid " entry"
msgid_plural " entries"
msgstr[0] " 条"

#: configdialog.cpp:131 configdialog.cpp:169
#, kde-format
msgid "Always save in history"
msgstr "总是保存到历史记录"

#: configdialog.cpp:135
#, kde-format
msgid "Text selection:"
msgstr "选中的文本内容："

#: configdialog.cpp:137 configdialog.cpp:175
#, kde-format
msgid "Only when explicitly copied"
msgstr "仅在明确复制时"

#: configdialog.cpp:142
#, kde-format
msgid "Whether text selections are saved in the clipboard history."
msgstr "是否将选中的文字内容保存到剪贴板的历史记录。"

#: configdialog.cpp:173
#, kde-format
msgid "Non-text selection:"
msgstr "选中的非文本内容："

#: configdialog.cpp:180
#, kde-format
msgid "Never save in history"
msgstr "永不保存到历史记录"

#: configdialog.cpp:185
#, kde-format
msgid ""
"Whether non-text selections (such as images) are saved in the clipboard "
"history."
msgstr "是否将选中的非文字内容 (如图像) 保存到剪贴板的历史记录。"

#: configdialog.cpp:259
#, kde-format
msgid "Show action popup menu:"
msgstr "显示操作弹出菜单："

#: configdialog.cpp:269
#, kde-kuit-format
msgctxt "@info"
msgid ""
"When text that matches an action pattern is selected or is chosen from the "
"clipboard history, automatically show the popup menu with applicable "
"actions. If the automatic menu is turned off here, or it is not shown for an "
"excluded window, then it can be shown by using the <shortcut>%1</shortcut> "
"key shortcut."
msgstr ""
"当选择的文本、或者从剪贴板历史选择的文本匹配一个操作模式时，自动显示包含可应"
"用操作的弹出菜单。如果在此关闭了自动菜单，或者因为位于已排除窗口而不显示时，"
"该菜单可以通过 <shortcut>%1</shortcut> 快捷键显示。"

#: configdialog.cpp:278
#, kde-format
msgid "Exclude Windows..."
msgstr "排除窗口..."

#: configdialog.cpp:292
#, kde-format
msgctxt "Unit of time"
msgid " second"
msgid_plural " seconds"
msgstr[0] " 秒"

#: configdialog.cpp:293
#, kde-format
msgctxt "No timeout"
msgid "None"
msgstr "无"

#: configdialog.cpp:302
#, kde-format
msgid "Options:"
msgstr "选项："

#: configdialog.cpp:329
#, kde-format
msgid "Exclude Windows"
msgstr "排除窗口"

#: configdialog.cpp:359
#, kde-kuit-format
msgctxt "@info"
msgid ""
"When a <interface>match pattern</interface> matches the clipboard contents, "
"its <interface>commands</interface> appear in the Klipper popup menu and can "
"be executed."
msgstr ""
"当<interface>匹配模式</interface>与剪贴板内容匹配时，可以执行它显示在 "
"Klipper 弹出菜单中的<interface>命令</interface>。"

#: configdialog.cpp:368
#, kde-format
msgctxt "@title:column"
msgid "Match pattern and commands"
msgstr "匹配模式和命令"

#: configdialog.cpp:368
#, kde-format
msgctxt "@title:column"
msgid "Description"
msgstr "描述"

#: configdialog.cpp:374
#, kde-format
msgid "Add Action..."
msgstr "添加操作..."

#: configdialog.cpp:378
#, kde-format
msgid "Edit Action..."
msgstr "编辑操作..."

#: configdialog.cpp:383
#, kde-format
msgid "Delete Action"
msgstr "删除操作"

#: configdialog.cpp:390
#, kde-kuit-format
msgctxt "@info"
msgid ""
"These actions appear in the popup menu which can be configured on the "
"<interface>Action Menu</interface> page."
msgstr ""
"弹出菜单中显示的操作可在<interface>操作菜单</interface>页面中进行配置。"

#: configdialog.cpp:574
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Delete the selected action <resource>%1</resource><nl/>and all of its "
"commands?"
msgstr "删除选中的操作 <resource>%1</resource><nl/>和它的所有命令吗？"

#: configdialog.cpp:575
#, kde-format
msgid "Confirm Delete Action"
msgstr "确认删除操作"

#: configdialog.cpp:604
#, kde-format
msgctxt "General Config"
msgid "General"
msgstr "常规"

#: configdialog.cpp:604
#, kde-format
msgid "General Configuration"
msgstr "常规配置"

#: configdialog.cpp:605
#, kde-format
msgctxt "Popup Menu Config"
msgid "Action Menu"
msgstr "操作菜单"

#: configdialog.cpp:605
#, kde-format
msgid "Action Menu"
msgstr "操作菜单"

#: configdialog.cpp:606
#, kde-format
msgctxt "Actions Config"
msgid "Actions Configuration"
msgstr "操作配置"

#: configdialog.cpp:606
#, kde-format
msgid "Actions Configuration"
msgstr "操作配置"

#: configdialog.cpp:609
#, kde-format
msgctxt "Shortcuts Config"
msgid "Shortcuts"
msgstr "快捷键"

#: configdialog.cpp:609
#, kde-format
msgid "Shortcuts Configuration"
msgstr "快捷键配置"

#: configdialog.cpp:690
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The action popup will not be shown automatically for these windows, even if "
"it is enabled. This is because, for example, a web browser may highlight a "
"URL in the address bar while typing, so the menu would show for every "
"keystroke.<nl/><nl/>If the action menu appears unexpectedly when using a "
"particular application, then add it to this list. <link>How to find the name "
"to enter</link>."
msgstr ""
"即使在已经启用操作弹出菜单的情况下，这些窗口也不会显示该菜单。例如在网页浏览"
"器时，地址栏可能会在输入 URL 的过程中高亮显示地址，从而导致每次按键输入时都要"
"弹出一次菜单。<nl/><nl/>如果某些应用程序中发生操作菜单意外弹出的情况，请将该"
"程序添加到此列表。<link>点击查看如何确定应用程序的正确名称</link>。"

#: configdialog.cpp:703
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"The name that needs to be entered here is the WM_CLASS name of the window to "
"be excluded. To find the WM_CLASS name for a window, in another terminal "
"window enter the command:<nl/><nl/>&nbsp;&nbsp;<icode>xprop | grep WM_CLASS</"
"icode><nl/><nl/>and click on the window that you want to exclude. The first "
"name that it displays after the equal sign is the one that you need to enter."
msgstr ""
"此处需要输入要排除窗口的 WM_CLASS 名称。要找到窗口的 WM_CLASS 名称，请新开一"
"个命令行终端窗口，输入以下命令：<nl/><nl/>&nbsp;&nbsp;<icode>xprop | grep "
"WM_CLASS</icode><nl/><nl/>然后点击需要排除的窗口。在等号后面显示的第一个名称"
"就是需要输入的名称。"

#: editactiondialog.cpp:34 editcommanddialog.cpp:89
#, kde-format
msgid "Ignore"
msgstr "忽略"

#: editactiondialog.cpp:36
#, kde-format
msgid "Replace Clipboard"
msgstr "替换剪贴板"

#: editactiondialog.cpp:38
#, kde-format
msgid "Add to Clipboard"
msgstr "添加到剪贴板"

#: editactiondialog.cpp:122
#, kde-format
msgid "Command"
msgstr "命令"

#: editactiondialog.cpp:124
#, kde-format
msgid "Output"
msgstr "输出"

#: editactiondialog.cpp:126
#, kde-format
msgid "Description"
msgstr "描述"

#: editactiondialog.cpp:179
#, kde-format
msgid "Action Properties"
msgstr "操作属性"

#: editactiondialog.cpp:191
#, kde-kuit-format
msgctxt "@info"
msgid ""
"An action takes effect when its <interface>match pattern</interface> matches "
"the clipboard contents. When this happens, the action's <interface>commands</"
"interface> appear in the Klipper popup menu; if one of them is chosen, the "
"command is executed."
msgstr ""
"操作会在它的<interface>匹配模式</interface>与剪贴板中的内容匹配时生效。当发现"
"匹配内容时，操作的<interface>命令</interface>将显示在 Klipper 的弹出菜单中；"
"选中一个显示的命令即可执行它。"

#: editactiondialog.cpp:203
#, kde-format
msgid "Enter a pattern to match against the clipboard"
msgstr "输入用于匹配剪贴板内容的模式"

#: editactiondialog.cpp:205
#, kde-format
msgid "Match pattern:"
msgstr "匹配模式："

#: editactiondialog.cpp:208
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The match pattern is a regular expression. For more information see the "
"<link url=\"https://en.wikipedia.org/wiki/Regular_expression\">Wikipedia "
"entry</link> for this topic."
msgstr ""
"匹配模式是一条正则表达式。详情可参考<link url=\"https://baike.baidu.com/item/"
"正则表达式/1700215\">百度百科相关页面</link>。"

#: editactiondialog.cpp:219
#, kde-format
msgid "Enter a description for the action"
msgstr "输入此操作的描述"

#: editactiondialog.cpp:220 editcommanddialog.cpp:83
#, kde-format
msgid "Description:"
msgstr "描述："

#: editactiondialog.cpp:223
#, kde-format
msgid "Include in automatic popup"
msgstr "包含在自动弹出菜单中"

#: editactiondialog.cpp:227
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The commands for this match will be included in the automatic action popup, "
"if it is enabled in the <interface>Action Menu</interface> page. If this "
"option is turned off, the commands for this match will not be included in "
"the automatic popup but they will be included if the popup is activated "
"manually with the <shortcut>%1</shortcut> key shortcut."
msgstr ""
"在匹配模式的<interface>操作菜单</interface>页面中启用相关选项后，它的命令将显"
"示在自动操作弹出菜单中。如果关闭相关选项，它的命令将不会显示在自动弹出菜单"
"中，但会在按下 <shortcut>%1</shortcut> 快捷键手动弹出菜单时显示。"

#: editactiondialog.cpp:262
#, kde-format
msgid "Add Command..."
msgstr "添加命令..."

#: editactiondialog.cpp:267
#, kde-format
msgid "Edit Command..."
msgstr "编辑命令..."

#: editactiondialog.cpp:273
#, kde-format
msgid "Delete Command"
msgstr "删除命令"

#: editactiondialog.cpp:388
#, kde-kuit-format
msgctxt "@info"
msgid "Delete the selected command <resource>%1</resource>?"
msgstr "删除选中的命令 <resource>%1</resource> 吗？"

#: editactiondialog.cpp:389
#, kde-format
msgid "Confirm Delete Command"
msgstr "确认删除命令"

#: editcommanddialog.cpp:46
#, kde-format
msgid "Command Properties"
msgstr "命令属性"

#: editcommanddialog.cpp:59
#, kde-format
msgid "Enter the command and arguments"
msgstr "输入命令和参数"

#: editcommanddialog.cpp:62
#, kde-format
msgid "Command:"
msgstr "命令："

#: editcommanddialog.cpp:71
#, kde-kuit-format
msgctxt "@info"
msgid ""
"A <placeholder>&#37;s</placeholder> in the command will be replaced by the "
"complete clipboard contents. <placeholder>&#37;0</placeholder> through "
"<placeholder>&#37;9</placeholder> will be replaced by the corresponding "
"captured texts from the match pattern."
msgstr ""
"命令中的 <placeholder>&#37;s</placeholder> 将被替换为对应的完整剪贴板内容。 "
"<placeholder>&#37;0</placeholder> 到 <placeholder>&#37;9</placeholder> 将被替"
"换为按照匹配模式捕获的文本。"

#: editcommanddialog.cpp:81
#, kde-format
msgid "Enter a description for the command"
msgstr "输入此命令的描述"

#: editcommanddialog.cpp:91
#, kde-format
msgid "Output from command:"
msgstr "命令的输出："

#: editcommanddialog.cpp:93
#, kde-format
msgid "Replace current clipboard"
msgstr "替换当前剪贴板"

#: editcommanddialog.cpp:97
#, kde-format
msgid "Append to clipboard"
msgstr "添加到剪贴板"

#: editcommanddialog.cpp:101
#, kde-format
msgid "What happens to the standard output of the command executed."
msgstr "在所执行命令的标准输出中发生的内容。"

#: editcommanddialog.cpp:115
#, kde-format
msgid "Reset the icon to the default for the command"
msgstr "重置图标为命令的默认值"

#: editcommanddialog.cpp:121
#, kde-format
msgid "Icon:"
msgstr "图标："

#: historyimageitem.cpp:36
#, kde-format
msgid "%1x%2 %3bpp"
msgstr "%1x%2 %3 位/像素"

#: klipper.cpp:137
#, kde-format
msgctxt "@action:inmenu Toggle automatic action"
msgid "Automatic Action Popup Menu"
msgstr "自动弹出操作菜单"

#: klipper.cpp:172
#, kde-format
msgctxt "@action:inmenu"
msgid "C&lear Clipboard History"
msgstr "清除剪贴板历史(&L)"

#: klipper.cpp:179
#, kde-format
msgctxt "@action:inmenu"
msgid "&Configure Klipper…"
msgstr "配置 Klipper(&C)…"

#: klipper.cpp:185
#, kde-format
msgctxt "@action:inmenu Quit Klipper"
msgid "&Quit"
msgstr "退出(&Q)"

#: klipper.cpp:190
#, kde-format
msgctxt "@action:inmenu"
msgid "Manually Invoke Action on Current Clipboard"
msgstr "在当前剪贴板上手动执行操作"

#: klipper.cpp:197
#, kde-format
msgctxt "@action:inmenu"
msgid "&Show Barcode…"
msgstr "显示条形码(&S)…"

#: klipper.cpp:206
#, kde-format
msgctxt "@action:inmenu"
msgid "Next History Item"
msgstr "下一条历史记录"

#: klipper.cpp:211
#, kde-format
msgctxt "@action:inmenu"
msgid "Previous History Item"
msgstr "上一条历史记录"

#: klipper.cpp:218
#, kde-format
msgctxt "@action:inmenu"
msgid "Show Items at Mouse Position"
msgstr "在鼠标位置显示项目"

#: klipper.cpp:229
#, kde-format
msgctxt "%1 is application display name"
msgid "%1 - Clipboard Tool"
msgstr "%1 - 剪贴板工具"

#: klipper.cpp:525
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can enable URL actions later in the <interface>Actions</interface> page "
"of the Clipboard applet's configuration window"
msgstr ""
"您可以稍后在剪贴板小程序的配置窗口中的<interface>操作</interface>页面启用 "
"URL 操作。"

#: klipper.cpp:564
#, kde-format
msgid "Should Klipper start automatically when you login?"
msgstr "您想要在登录时自动启动 Klipper 吗？"

#: klipper.cpp:565
#, kde-format
msgid "Automatically Start Klipper?"
msgstr "自动启动 Klipper 吗？"

#: klipper.cpp:566
#, kde-format
msgid "Start"
msgstr "启动"

#: klipper.cpp:567
#, kde-format
msgid "Do Not Start"
msgstr "不启动"

#: klipper.cpp:938
#, kde-format
msgid "Mobile Barcode"
msgstr "移动条形码"

#: klipper.cpp:985
#, kde-format
msgid "Do you really want to clear and delete the entire clipboard history?"
msgstr "您确定要清除剪贴板的所有历史记录吗？"

#: klipper.cpp:986
#, kde-format
msgid "Clear Clipboard History"
msgstr "清除剪贴板历史记录"

#: klipper.cpp:1002 klipper.cpp:1011
#, kde-format
msgid "Clipboard history"
msgstr "剪贴板历史"

#: klipper.cpp:1028
#, kde-format
msgid "up"
msgstr "向上"

#: klipper.cpp:1035
#, kde-format
msgid "current"
msgstr "当前"

#: klipper.cpp:1042
#, kde-format
msgid "down"
msgstr "向下"

#. i18n: ectx: label, entry (Version), group (General)
#: klipper.kcfg:10
#, kde-format
msgid "Klipper version"
msgstr "Klipper 版本"

#. i18n: ectx: label, entry (KeepClipboardContents), group (General)
#: klipper.kcfg:13
#, kde-format
msgid "Save history across desktop sessions"
msgstr "跨桌面会话保存历史记录"

#. i18n: ectx: tooltip, entry (KeepClipboardContents), group (General)
#: klipper.kcfg:15
#, kde-format
msgid ""
"Retain the clipboard history, so it will be available the next time you log "
"in."
msgstr "保留剪贴板历史记录，以便下次登录时继续使用。"

#. i18n: ectx: label, entry (PreventEmptyClipboard), group (General)
#: klipper.kcfg:18
#, kde-format
msgid "Prevent the clipboard from being cleared"
msgstr "防止清空剪贴板"

#. i18n: ectx: whatsthis, entry (PreventEmptyClipboard), group (General)
#: klipper.kcfg:20
#, kde-format
msgid ""
"Do not allow the clipboard to be cleared, for example when an application "
"exits."
msgstr "不允许清除剪贴板内容，例如在应用程序退出时。"

#. i18n: ectx: label, entry (SyncClipboards), group (General)
#: klipper.kcfg:27
#, kde-format
msgid "Keep the selection and clipboard the same"
msgstr "保持选中内容和剪贴板内容一致"

#. i18n: ectx: whatsthis, entry (SyncClipboards), group (General)
#: klipper.kcfg:29
#, kde-format
msgid ""
"Content selected with the cursor is automatically copied to the clipboard so "
"that it can be pasted with either a Paste action or a middle-click.<br/><a "
"href=\"1\">More about the selection and clipboard</a>."
msgstr ""
"通过光标选中的内容将被自动复制到剪贴板，以便在之后通过粘贴操作或者鼠标中间键"
"进行粘贴。<br/><a href=\"1\">查看关于选中内容和剪贴板内容的更多信息</a>。"

#. i18n: ectx: label, entry (IgnoreSelection), group (General)
#: klipper.kcfg:32
#, kde-format
msgid "Ignore the selection"
msgstr "忽略选中内容"

#. i18n: ectx: whatsthis, entry (IgnoreSelection), group (General)
#: klipper.kcfg:34
#, kde-format
msgid ""
"Content selected with the cursor but not explicitly copied to the clipboard "
"is not automatically stored in the clipboard history, and can only be pasted "
"using a middle-click."
msgstr ""
"使用光标选中，但没有明确复制到剪贴板的内容不会自动保存到剪贴板历史记录中，而"
"且只能通过点击鼠标中间键进行粘贴。"

#. i18n: ectx: label, entry (SelectionTextOnly), group (General)
#: klipper.kcfg:37
#, kde-format
msgid "Text selection only"
msgstr "仅接受纯文本选中内容"

#. i18n: ectx: whatsthis, entry (SelectionTextOnly), group (General)
#: klipper.kcfg:39
#, kde-format
msgid ""
"Only store text selections in the clipboard history, not images or any other "
"type of data."
msgstr "仅保存选中的文字内容到剪贴板历史记录，不保存图像或者其他类型的数据。"

#. i18n: ectx: label, entry (IgnoreImages), group (General)
#: klipper.kcfg:42
#, kde-format
msgid "Always ignore images"
msgstr "总是忽略图像"

#. i18n: ectx: whatsthis, entry (IgnoreImages), group (General)
#: klipper.kcfg:44
#, kde-format
msgid ""
"Do not store images in the clipboard history, even if explicitly copied."
msgstr "不要保存图像到剪贴板历史记录，即使图像已被明确复制。"

#. i18n: ectx: label, entry (UseGUIRegExpEditor), group (General)
#: klipper.kcfg:47
#, kde-format
msgid "Use graphical regexp editor"
msgstr "使用图形化正则表达式编辑器"

#. i18n: ectx: label, entry (URLGrabberEnabled), group (General)
#: klipper.kcfg:51
#, kde-format
msgid "Immediately on selection"
msgstr "选中后立即弹出"

#. i18n: ectx: tooltip, entry (URLGrabberEnabled), group (General)
#: klipper.kcfg:52
#, kde-format
msgid ""
"Show the popup menu of applicable actions as soon as a selection is made."
msgstr "选择对象后立即显示包含可应用操作的弹出菜单。"

#. i18n: ectx: label, entry (NoActionsForWM_CLASS), group (General)
#: klipper.kcfg:57
#, kde-format
msgid "No actions for WM_CLASS"
msgstr "没有对应 WM_CLASS 的操作"

#. i18n: ectx: label, entry (TimeoutForActionPopups), group (General)
#: klipper.kcfg:60
#, kde-format
msgid "Automatic action menu time:"
msgstr "自动操作菜单弹出时间："

#. i18n: ectx: tooltip, entry (TimeoutForActionPopups), group (General)
#: klipper.kcfg:64
#, kde-format
msgid "Display the automatic action popup menu for this time."
msgstr "设置显示自动操作弹出菜单的时间。"

#. i18n: ectx: label, entry (MaxClipItems), group (General)
#: klipper.kcfg:67
#, kde-format
msgid "History size:"
msgstr "历史记录数量："

#. i18n: ectx: tooltip, entry (MaxClipItems), group (General)
#: klipper.kcfg:71
#, kde-format
msgid "The clipboard history will store up to this many items."
msgstr "剪贴板历史记录保存的记录条数。"

#. i18n: ectx: label, entry (ActionList), group (General)
#: klipper.kcfg:74
#, kde-format
msgid "Dummy entry for indicating changes in an action's tree widget"
msgstr "在操作的树形部件中用于表示改动的空条目"

#. i18n: ectx: label, entry (StripWhiteSpace), group (Actions)
#: klipper.kcfg:84
#, kde-format
msgid "Trim whitespace from selection"
msgstr "去除选中文本的空格"

#. i18n: ectx: whatsthis, entry (StripWhiteSpace), group (Actions)
#: klipper.kcfg:86
#, kde-format
msgid ""
"Remove any whitespace from the start and end of selected text, before "
"performing an action. For example, this ensures that a URL pasted in a "
"browser is interpreted as expected. The text saved on the clipboard is not "
"affected."
msgstr ""
"在进行操作之前移除位于选中文本内容开头和结尾的空格。例如这样做可以确保粘贴的 "
"URL 能被浏览器正确解析。保存在剪贴板中的文本不受影响。"

#. i18n: ectx: label, entry (ReplayActionInHistory), group (Actions)
#: klipper.kcfg:89
#, kde-format
msgid "For an item chosen from history"
msgstr "在历史记录中选择项目时弹出"

#. i18n: ectx: tooltip, entry (ReplayActionInHistory), group (Actions)
#: klipper.kcfg:91
#, kde-format
msgid ""
"Show the popup menu of applicable actions if an entry is chosen from the "
"clipboard history."
msgstr "在剪贴板历史记录中选择条目时，显示可应用操作的弹出菜单。"

#. i18n: ectx: label, entry (EnableMagicMimeActions), group (Actions)
#: klipper.kcfg:94
#, kde-format
msgid "Include MIME actions"
msgstr "包含 MIME 操作"

#. i18n: ectx: whatsthis, entry (EnableMagicMimeActions), group (Actions)
#: klipper.kcfg:96
#, kde-format
msgid ""
"If a file name or URL is selected, include applications that can accept its "
"MIME type in the popup menu."
msgstr ""
"如果选择的是文件名或 URL，在弹出菜单中包含能够接受该 MIME 类型的应用程序。"

#: klipperpopup.cpp:105
#, kde-format
msgctxt "%1 is application display name"
msgid "%1 - Clipboard Items"
msgstr "%1 - 剪贴板项目"

#: klipperpopup.cpp:109
#, kde-format
msgid "Search…"
msgstr "搜索…"

#: klipperpopup.cpp:167
#, kde-format
msgid "Invalid regular expression, %1"
msgstr "无效的正则表达式，%1"

#: klipperpopup.cpp:172
#, kde-format
msgid "Clipboard is empty"
msgstr "剪贴板为空"

#: klipperpopup.cpp:174
#, kde-format
msgid "No matches"
msgstr "无匹配条目"

#: main.cpp:27
#, kde-format
msgid "Klipper"
msgstr "Klipper"

#: main.cpp:29
#, kde-format
msgid "Plasma cut & paste history utility"
msgstr "Plasma 剪切和粘贴历史工具"

#: main.cpp:31
#, kde-format
msgid ""
"(c) 1998, Andrew Stanley-Jones\n"
"1998-2002, Carsten Pfeiffer\n"
"2001, Patrick Dubroy"
msgstr ""
"(c) 1998, Andrew Stanley-Jones\n"
"1998-2002, Carsten Pfeiffer\n"
"2001, Patrick Dubroy"

#: main.cpp:34
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: main.cpp:34
#, kde-format
msgid "Author"
msgstr "作者"

#: main.cpp:36
#, kde-format
msgid "Andrew Stanley-Jones"
msgstr "Andrew Stanley-Jones"

#: main.cpp:36
#, kde-format
msgid "Original Author"
msgstr "原始作者"

#: main.cpp:38
#, kde-format
msgid "Patrick Dubroy"
msgstr "Patrick Dubroy"

#: main.cpp:38
#, kde-format
msgid "Contributor"
msgstr "贡献者"

#: main.cpp:40
#, kde-format
msgid "Luboš Luňák"
msgstr "Luboš Luňák"

#: main.cpp:40
#, kde-format
msgid "Bugfixes and optimizations"
msgstr "程序缺陷修复和优化"

#: main.cpp:42
#, kde-format
msgid "Esben Mose Hansen"
msgstr "Esben Mose Hansen"

#: main.cpp:42
#, kde-format
msgid "Previous Maintainer"
msgstr "前任维护者"

#: main.cpp:44
#, kde-format
msgid "Martin Gräßlin"
msgstr "Martin Gräßlin"

#: main.cpp:44
#, kde-format
msgid "Maintainer"
msgstr "维护者"

#: main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE 中国, Guo Yunhe, Tyson Tan"

#: main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-china@kde.org, i@guoyunhe.me, tysontan@tysontan.com"

#: popupproxy.cpp:145
#, kde-format
msgid "&More"
msgstr "更多(&M)"

#: urlgrabber.cpp:200
#, kde-format
msgid "Disable This Popup"
msgstr "禁用此弹出菜单"

#: urlgrabber.cpp:206
#, kde-format
msgid "&Cancel"
msgstr "取消(&C)"
