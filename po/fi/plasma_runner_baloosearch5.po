# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Lasse Liehu <lasse.liehu@gmail.com>, 2014, 2016.
# Tommi Nieminen <translator@legisign.org>, 2018, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-09 01:04+0000\n"
"PO-Revision-Date: 2023-03-04 18:23+0200\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: baloosearchrunner.cpp:65
#, kde-format
msgid "Open Containing Folder"
msgstr "Avaa tiedostosijainti"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Audios"
msgstr "Äänitiedostot"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Images"
msgstr "Kuvat"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Videos"
msgstr "Videot"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Spreadsheets"
msgstr "Laskentataulukot"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Presentations"
msgstr "Esitykset"

#: baloosearchrunner.cpp:92
#, kde-format
msgid "Folders"
msgstr "Kansiot"

# Tässä ei oikein voi kääntää toisin, koska Files on jo ”Tiedostot”
#: baloosearchrunner.cpp:93
#, kde-format
msgid "Documents"
msgstr "Asiakirjat"

#: baloosearchrunner.cpp:94
#, kde-format
msgid "Archives"
msgstr "Arkistot"

#: baloosearchrunner.cpp:95
#, kde-format
msgid "Texts"
msgstr "Tekstitiedostot"

#: baloosearchrunner.cpp:96
#, kde-format
msgid "Files"
msgstr "Tiedostot"

#~ msgid "Search through files, emails and contacts"
#~ msgstr "Etsi tiedostoista, sähköposteista ja yhteystiedoista"

#~ msgid "Email"
#~ msgstr "Sähköposti"
