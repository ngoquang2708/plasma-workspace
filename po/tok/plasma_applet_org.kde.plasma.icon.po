# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-26 01:59+0000\n"
"PO-Revision-Date: 2022-07-08 04:45+0000\n"
"Last-Translator: Weblate Admin <admin@example.com>\n"
"Language-Team: Toki Pona <http://weblate.blackquill.cc/projects/ante-toki-pi-"
"ilo-kde/ilo-lili-pi-sitelen-lili-taso/tok/>\n"
"Language: tok\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || n%10>=5 && n%10<=9 || n"
"%100>=11 && n%100<=14 ? 2 : 3;\n"
"X-Generator: Weblate 4.10.1\n"

#: iconapplet.cpp:93
#, kde-format
msgid "Failed to create icon widgets folder '%1'"
msgstr ""

#: iconapplet.cpp:139
#, kde-format
msgid "Failed to copy icon widget desktop file from '%1' to '%2'"
msgstr ""

#: iconapplet.cpp:386
#, kde-format
msgid "Open Containing Folder"
msgstr "o open e poki pi jo lipu ni"

#: iconapplet.cpp:567
#, kde-format
msgid "Properties for %1"
msgstr ""

#: package/contents/ui/main.qml:70
#, kde-format
msgid "Properties"
msgstr "ijo lipu"
