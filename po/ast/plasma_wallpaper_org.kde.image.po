# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Enol P. <enolp@softastur.org>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-05 02:19+0000\n"
"PO-Revision-Date: 2023-05-06 22:00+0200\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: \n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Softastur"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "alministradores@softastur.org"

#: imagepackage/contents/ui/config.qml:94
#, kde-format
msgid "Positioning:"
msgstr ""

#: imagepackage/contents/ui/config.qml:97
#, kde-format
msgid "Scaled and Cropped"
msgstr ""

#: imagepackage/contents/ui/config.qml:101
#, kde-format
msgid "Scaled"
msgstr ""

#: imagepackage/contents/ui/config.qml:105
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr ""

#: imagepackage/contents/ui/config.qml:109
#, kde-format
msgid "Centered"
msgstr ""

#: imagepackage/contents/ui/config.qml:113
#, kde-format
msgid "Tiled"
msgstr ""

#: imagepackage/contents/ui/config.qml:141
#, kde-format
msgid "Background:"
msgstr ""

#: imagepackage/contents/ui/config.qml:142
#, kde-format
msgid "Blur"
msgstr ""

#: imagepackage/contents/ui/config.qml:151
#, kde-format
msgid "Solid color"
msgstr ""

#: imagepackage/contents/ui/config.qml:161
#, kde-format
msgid "Select Background Color"
msgstr ""

#: imagepackage/contents/ui/config.qml:207
#, kde-format
msgid "Add Image…"
msgstr ""

#: imagepackage/contents/ui/config.qml:213
#: slideshowpackage/contents/ui/SlideshowComponent.qml:257
#, kde-format
msgid "Get New Wallpapers…"
msgstr ""

#: imagepackage/contents/ui/main.qml:66
#, kde-format
msgid "Open Wallpaper Image"
msgstr ""

#: imagepackage/contents/ui/main.qml:67
#, kde-format
msgid "Next Wallpaper Image"
msgstr ""

#: imagepackage/contents/ui/WallpaperDelegate.qml:33
#, kde-format
msgid "Open Containing Folder"
msgstr ""

#: imagepackage/contents/ui/WallpaperDelegate.qml:39
#, kde-format
msgid "Restore wallpaper"
msgstr ""

#: imagepackage/contents/ui/WallpaperDelegate.qml:44
#, kde-format
msgid "Remove Wallpaper"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr ""

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""

#: plugin/imagebackend.cpp:243
#, kde-format
msgid "Directory with the wallpaper to show slides from"
msgstr ""

#: plugin/imagebackend.cpp:353
#, kde-format
msgid "Open Image"
msgstr ""

#: plugin/imagebackend.cpp:353
#, kde-format
msgid "Image Files"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:38
#, kde-format
msgid "Order:"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:45
#, kde-format
msgid "Random"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:49
#, kde-format
msgid "A to Z"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:53
#, kde-format
msgid "Z to A"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:57
#, kde-format
msgid "Date modified (newest first)"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:61
#, kde-format
msgid "Date modified (oldest first)"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:86
#, kde-format
msgid "Group by folders"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:98
#, kde-format
msgid "Change every:"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:108
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 hora"
msgstr[1] "%1 hores"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:128
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minutu"
msgstr[1] "%1 minutos"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:148
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 segundu"
msgstr[1] "%1 segundos"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:162
#, kde-format
msgid "Folders"
msgstr "Carpetes"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:220
#, kde-format
msgid "Remove Folder"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:225
#, kde-format
msgid "Open Folder"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:235
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:250
#, kde-format
msgid "Add Folder…"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:47
#, kde-format
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:50
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:56
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:62
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:98
#, kde-kuit-format
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:111
#, kde-format
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr ""
