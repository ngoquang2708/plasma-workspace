# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Yoann Laissus <yoann.laissus@gmail.com>, 2015.
# Vincent Pinon <vpinon@kde.org>, 2016.
# Simon Depiets <sdepiets@gmail.com>, 2018.
# Xavier Besnard <xavier.besnard@neuf.fr>, 2020, 2021, 2022.
#
# Xavier BESNARD <xavier.besnard]neuf.fr>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-28 02:18+0000\n"
"PO-Revision-Date: 2023-01-16 18:22+0100\n"
"Last-Translator: Xavier BESNARD <xavier.besnard]neuf.fr>\n"
"Language-Team: fr\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 22.12.1\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Xavier Besnard"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xavier.besnard@neuf.fr"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Vacances"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Évènements"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Tâche à faire"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Autre"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "Évènement %1"
msgstr[1] "%1 évènements"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "No events"
msgstr "Aucun évènement"

#: calendar/qml/MonthViewHeader.qml:67
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 %2"

#: calendar/qml/MonthViewHeader.qml:113
#, kde-format
msgid "Days"
msgstr "Jours"

#: calendar/qml/MonthViewHeader.qml:119
#, kde-format
msgid "Months"
msgstr "Mois"

#: calendar/qml/MonthViewHeader.qml:125
#, kde-format
msgid "Years"
msgstr "Années"

#: calendar/qml/MonthViewHeader.qml:163
#, kde-format
msgid "Previous Month"
msgstr "Mois précédent"

#: calendar/qml/MonthViewHeader.qml:165
#, kde-format
msgid "Previous Year"
msgstr "Année précédente"

#: calendar/qml/MonthViewHeader.qml:167
#, kde-format
msgid "Previous Decade"
msgstr "Décennie précédente"

#: calendar/qml/MonthViewHeader.qml:184
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Aujourd'hui"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgid "Reset calendar to today"
msgstr "Réinitialiser le calendrier à aujourd'hui"

#: calendar/qml/MonthViewHeader.qml:196
#, kde-format
msgid "Next Month"
msgstr "Mois suivant"

#: calendar/qml/MonthViewHeader.qml:198
#, kde-format
msgid "Next Year"
msgstr "Année suivante"

#: calendar/qml/MonthViewHeader.qml:200
#, kde-format
msgid "Next Decade"
msgstr "Décennie suivante"

#: calendar/qml/MonthViewHeader.qml:256
#, kde-format
msgid "Keep Open"
msgstr "Conserver ouvert"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Configurer..."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Verrouillage d'écran activé"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Définit si l'écran sera verrouillé après un temps spécifié."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Temps d'expiration de l'économiseur d'écran"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Définit le nombre de minutes après lequel l'écran est verrouillé."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Nouvelle session"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filtres"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:41
#, kde-format
msgid "Select Plasmoid File"
msgstr "Sélectionner le fichier de composant graphique"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:65
#, kde-format
msgid "Installing the package %1 failed."
msgstr "L'installation du paquet %1 a échoué."

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:65
#, kde-format
msgid "Installation Failure"
msgstr "Échec de l'installation"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Accessibility"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Application Launchers"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Astronomy"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Date and Time"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Development Tools"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Education"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Environment and Weather"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "Examples"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
msgctxt "applet category"
msgid "File System"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
#, fuzzy
#| msgctxt "NAME OF TRANSLATORS"
#| msgid "Your names"
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Xavier Besnard"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Graphics"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Language"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Mapping"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Miscellaneous"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Multimedia"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Online Services"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "Productivity"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "System Information"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Utilities"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Clipboard"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:133
msgctxt "applet category"
msgid "Tasks"
msgstr ""

#: shellprivate/widgetexplorer/widgetexplorer.cpp:149
#, kde-format
msgid "All Widgets"
msgstr "Tous les composants graphiques"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:153
#, kde-format
msgid "Running"
msgstr "En cours d'exécution"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:159
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Non installable"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:163
#, kde-format
msgid "Categories:"
msgstr "Catégories :"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:235
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Télécharger de nouveaux composants graphiques"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:244
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Installer un composant graphique depuis un fichier local..."

#~ msgid "&Execute"
#~ msgstr "&Exécuter"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Modèles"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Console de langage de script de l'environnement de bureau"

#~ msgid "Editor"
#~ msgstr "Éditeur"

#~ msgid "Load"
#~ msgstr "Charger"

#~ msgid "Use"
#~ msgstr "Utiliser"

#~ msgid "Output"
#~ msgstr "Sortie"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "Impossible de charger le fichier de script <b>%1</b>"

#~ msgid "Open Script File"
#~ msgstr "Ouvrir le fichier de script"

#~ msgid "Save Script File"
#~ msgstr "Enregistrer le fichier de script"

#~ msgid "Executing script at %1"
#~ msgstr "Exécuter le script à %1"

#~ msgid "Runtime: %1ms"
#~ msgstr "Exécution : %1 ms"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Télécharger des modules externes de fonds d'écran"
